import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public http: HttpClient) { }


  create(body) {
    let url = "http://localhost:3000/users/create"
    let header = {
      "Content-Type": "application/json"
    }
    return this.http.post(url, body, { headers: header })
  }

  login(body) {
    let url = "http://localhost:3000/users/userFind"
    let header = {
      "Content-Type": "application/json"
    }
    return this.http.post(url, body, { headers: header })
  }

  chang(body) {
    let url = "http://localhost:3000/users/chang"
    let header = {
      "Content-Type": "application/json"
    }
    return this.http.post(url, body, { headers: header })
  }

  updateProfile(body) {
    let url = "http://localhost:3000/users/update"
    let header = {
      "Content-Type": "application/json"
    }
    return this.http.post(url, body, { headers: header })
  }


}
