import { Component } from '@angular/core';
import { IonItem } from '@ionic/angular';
import { ApiService } from './../api.service';
import { AlertController,Platform } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private api: ApiService,
    public plt: Platform,
    private alertController: AlertController,
    private router: Router
  ) {}

  user:any
  FileImg 
  imgElem
  imageUrl
  selectedImage
  checkLearning

  fname
  lname
  userName
  passWord

  keyCode
  passwordtest
  checkLoop 
  loop=[]
  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("user")))
    this.user = JSON.parse(localStorage.getItem("user"))
    this.fname = this.user['fname']
    this.lname =  this.user['lname']
    this.imageUrl = this.user['img']
    this.userName = this.user['userName']
   this.passWord = this.user['passWord'][this.user['passWord'].length-1]
    

  }

  checkKey(keyCode) {
    console.log(keyCode);
    this.keyCode = keyCode
  }

  check(){
    console.log(this.passwordtest.length);
      
    console.log(this.checkLoop);
    console.log(this.passwordtest.length);
    if(this.passwordtest.length >= this.checkLoop){
      this.loop.push(this.keyCode)
    }else{   
      this.loop.pop()
    }
    console.log(this.loop);
    this.checkLoop = this.passwordtest.length
    }

  onImageSelected(event) {

    this.selectedImage = event.target.files[0];
    let reader = new FileReader();

    reader.onload = (e: any) => {
      this.imageUrl = e.target.result;
     
    };
    reader.readAsDataURL(this.selectedImage);    
  }

  testA(){
    let check = 0
    let loopKey = 0
    this.checkLearning = false
    this.loop.forEach(item => {   
      if((check+1) == item){
        loopKey=loopKey+1
        if(loopKey == 2){
          this.checkLearning = true
        }
      }else{
        loopKey=0
      }
      check = item
    });
    console.log(this.checkLearning);
   if(this.checkLearning){
    this.alert4()
   }else{
     this.edit()
   }
  }

  edit(){
    let body ={}
       body['userName'] = this.userName
    if(this.fname != null || this.fname != this.user['fname']){
       body['fname'] = this.fname
    }
    if(this.lname != null || this.lname != this.user['lname']){
      body['lname'] = this.lname
    }
    if(this.passWord != null || this.passWord != this.user['passWord']){
      if(this.passWord.length <= 5){
        this.alert3()
        return
      }
      body['passWord'] = this.passWord
    }
    if(this.imageUrl != null || this.imageUrl != this.user['img']){
      body['img'] = this.imageUrl
    }
    console.log(body);

    this.api.updateProfile(body).subscribe(res =>{
      if(res['status']){
        this.alert1()
      }else{

      }
    })
  }

  goToLogin(){
    localStorage.clear()
    this.router.navigate(['/login'])
  }

  async alert1(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'แก้ไขสำเร็จ',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
          this.router.navigate(['/login'])
        }
      }]
    });
    (await alert).present();
  }

  async alert2(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'ผิดพลาด',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

  
  async alert3(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'PassWord ต้องมากกว่า 6  ตัวอักษร',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

  async alert4(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'PassWord มีการเรียงอักษร',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }


  
}
