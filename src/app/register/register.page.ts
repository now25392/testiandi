import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { ApiService } from './../api.service';
import { AlertController,Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import * as CryptoJs from 'crypto-js'

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private api: ApiService,
    private alertController: AlertController,
    private router: Router
  ) { }

  test = "A"
  passwordtest =""
  checkLearning = false
  checkLoop = 0
  loop=[]
  keyCode
  FileImg

  userName
  passWord
  fname
  lname
  selectedImage: any;
  imageUrl: any = "assets/icon/profile-user.png";
  
  height:any
  bannerImage = document.getElementById('bannerImg');
  

  ngOnInit() {
    console.log(String.fromCharCode())
  }

  checkKey(keyCode) {
    console.log(keyCode);
    this.keyCode = keyCode
  }

  check(){
  console.log(this.passwordtest.length);
    
  console.log(this.checkLoop);
  console.log(this.passwordtest.length);
  if(this.passwordtest.length >= this.checkLoop){
    this.loop.push(this.keyCode)
  }else{   
    this.loop.pop()
  }
  console.log(this.loop);
  this.checkLoop = this.passwordtest.length
  }  
  
  testA(){
    let check = 0
    let loopKey = 0
    this.checkLearning = false
    this.loop.forEach(item => {   
      if((check+1) == item){
        loopKey=loopKey+1
        if(loopKey == 2){
          this.checkLearning = true
        }
      }else{
        loopKey=0
      }
      check = item
    });
    console.log(this.checkLearning);
    console.log(loopKey);
    console.log(this.FileImg);
    console.log( this.imageUrl);
    if(this.checkLearning){
      this.alert2()
    }else{
      this.apiSend()
    }
  }

  onImageSelected(event) {

    this.selectedImage = event.target.files[0];
    let reader = new FileReader();

    reader.onload = (e: any) => {
      this.imageUrl = e.target.result;
     
    };
    reader.readAsDataURL(this.selectedImage);    
  }


  apiSend(){
    if(this.passWord.length <= 5){
      this.alert1()
      return
    }
    let passChang
    let pass ={
      pass:this.passWord
    }
   
    this.api.chang(pass).subscribe(res =>{
      passChang = res['message']
      let body = {
        userName: this.userName,
        passWord:[passChang],
        fname: this.fname,
        lname: this.lname,
        img: this.imageUrl
      }
      
      this.api.create(body).subscribe(res =>{
        console.log(res);
        if(res['status']){
          this.alert3()
        }else{
          this.alert4()
        }
      })
    })
  
 
  }

  goToLogin(){
    this.router.navigate(['/login'])
  }
  async alert1(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'PassWord ต้องมากกว่า 6  ตัวอักษร',
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

  async alert2(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'PassWord มีการเรียงตัวอักษร',
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

  async alert3(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'สมัครสำเร็จ',
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
          this.router.navigate(['/login'])
        }
      }]
    });
    (await alert).present();
  }

  async alert4(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'id ซ้ำ',
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }





}
