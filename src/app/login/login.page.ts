import { Component, OnInit } from '@angular/core';
import { AlertController,Platform } from '@ionic/angular';
import { ApiService } from './../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private api: ApiService,
    public plt: Platform,
    private alertController: AlertController,
    private router: Router
  ) { }

  test = "A"
  passwordtest =""
  checkLearning = false
  checkLoop = 0
  loop=[]
  keyCode

  userName
  passWord

  dataToEncrypt:any={id:'abc123',name:'demoname'}
  encyptedData=""
  secretKey="mypassword"


  ngOnInit() {
    this.Encrypt()
    console.log(this.encyptedData)
  }

  Encrypt(){

  }

  checkKey(keyCode) {
    console.log(keyCode);
    this.keyCode = keyCode
  }
  check(){
  console.log(this.passwordtest.length);
    
  console.log(this.checkLoop);
  console.log(this.passwordtest.length);
  if(this.passwordtest.length >= this.checkLoop){
    this.loop.push(this.keyCode)
  }else{   
    this.loop.pop()
  }
  console.log(this.loop);
  this.checkLoop = this.passwordtest.length
  }
  
  testA(){
    let check = 0
    let loopKey = 0
    this.checkLearning = false
    this.loop.forEach(item => {   
      if((check+1) == item){
        loopKey=loopKey+1
        if(loopKey == 2){
          this.checkLearning = true
        }
      }else{
        loopKey=0
      }
      check = item
    });
    console.log(this.checkLearning);
    console.log(loopKey);
  }

  login(){
    if(this.userName == null && this.passWord == null){
      this.alert1()
      return
    }

    let body = {
      "userName":this.userName,
      "passWord":this.passWord
    }
    this.api.login(body).subscribe( item =>{
      console.log(item);
      if(item['status']){
        localStorage.setItem("user",JSON.stringify(item['user']))
        this.router.navigate(['/home'])
      }else{
        this.alert2()
      }
    })
  }

  goToRegister(){
    this.router.navigate(['/register'])
  }


  async alert1(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'กรุณาใส่id หรือ password',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

  async alert2(){
    let alert = this.alertController.create({
      cssClass: 'basic-alert',
      header: "แจ้งเตือน",
      message: 'password หรือ idไม่ถูกต้อง',
    
      buttons: [
        {
        text: 'ตกลง',
        handler: () => {
        }
      }]
    });
    (await alert).present();
  }

}
